from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QPushButton, QMessageBox
from PyQt5 import uic, QtCore
from models.products_model import ProductsModel
from controllers.products_form import ProductsForm
import pathlib

class Products(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        route = pathlib.Path(__file__).parent.parent
        uic.loadUi(route/"views/products.ui", self)
        self.__product_model = ProductsModel()
        self._productsForm = ProductsForm()
        self.load_products()
        self.newProductAction.triggered.connect(lambda: self.create_product())
        self._productsForm.product_saved.connect(self.load_products)

    #Función para cargar los datos en la tabla de categorías
    def load_products(self):
        self.productsTable.clearContents()
        products_list = self.__product_model.get_products()  
        self.productsTable.setRowCount(len(products_list))
        for i, product in enumerate(products_list):
            id_producto, nombre_producto, precio, nombre_categoria = product
            self.productsTable.setItem(i, 0, QTableWidgetItem(str(id_producto)))
            self.productsTable.setItem(i, 1, QTableWidgetItem(str(nombre_producto)))
            self.productsTable.setItem(i, 2, QTableWidgetItem(str(precio)))
            self.productsTable.setItem(i, 3, QTableWidgetItem(str(nombre_categoria)))

            #Boton de Edición
            edit_button = QPushButton("Editar")
            edit_button.clicked.connect(self.edit_product)
            edit_button.setProperty("row", i)
            self.productsTable.setCellWidget(i, 4, edit_button)

            # Boton de Eliminar
            delete_button = QPushButton("Eliminar")
            delete_button.clicked.connect(self.delete_product_data)
            delete_button.setProperty("row", i)
            self.productsTable.setCellWidget(i, 5, delete_button)

    #Función de edición de categorías
    def edit_product(self):
        sender = self.sender()
        row = sender.property("row")
        id_product = self.productsTable.item(row, 0).text()
        self._productsForm.load_product_data(id_product)
        self._productsForm.show()

    # Función que prepara y muestra el formulario de creación de categorías
    def create_product(self):
        self._productsForm.reset_form()
        self._productsForm.show()

    # Función para eliminar una categoría y su respectivo mensaje de confirmación
    def delete_product_data(self):
        button = self.sender()
        row = self.productsTable.indexAt(button.pos()).row()
        id_product = self.productsTable.item(row, 0).text()
        msg = QMessageBox.question(
            self, 'Confirmar Eliminación', '¿Estás seguro que quieres eliminar este producto?',
            QMessageBox.Yes | QMessageBox.No
        )
        if msg == QMessageBox.Yes:
            try:
                # Asegúrate de no cerrar el cursor antes de este llamado
                if self.__product_model.delete_product(id_product):
                    self.load_products()
                else:
                    QMessageBox.warning(self, 'Error', 'Hubo un problema al intentar eliminar el producto.')
            except Exception as e:
                QMessageBox.critical(self, 'Error', f'Error al eliminar el producto: {str(e)}')

    
    #Función para cerrar la conexión
    def closeEvent(self, ev) -> None:
        self.__product_model.close()
