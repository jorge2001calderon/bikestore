from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem
from PyQt5.QtCore import pyqtSignal
from PyQt5 import uic
import pathlib
from models.products_model import ProductsModel

class ProductsForm(QMainWindow):
    product_saved = pyqtSignal()

    def __init__(self) -> None:
        super().__init__()
        self._product_model = ProductsModel() 
        self._product_id = None
        route = pathlib.Path(__file__).parent.parent
        uic.loadUi(route / "views/products_form.ui", self)
        self.saveButton.clicked.connect(self.save_product)
        self.cancelButton.clicked.connect(self.close)
        self.comboBoxCategory.addItems([category[1] for category in self._product_model.get_categories()])


    # Función para guardar las categorías
    def save_product(self):
        try:
            if self._product_id:
                self._product_model.update_product(
                    self._product_id,
                    self.nameProductTextField.text(),
                    self.priceProductTextField.text(),
                    self.get_selected_category_id()
                )
            else:
                self._product_model.create_product(
                    self.nameProductTextField.text(),
                    self.priceProductTextField.text(),
                    self.get_selected_category_id()
                )

            # Emite la señal de producto guardado y cierra el formulario
            self.product_saved.emit()
            self.close()
        except Exception as e:
            print(f"Error al guardar el producto: {str(e)}")

    
    # Función para leer la información de los productos y cargar en el formulario
    def load_products(self):
        self.productsTable.clearContents()
        products_list = self._product_model.get_products()
        self.productsTable.setRowCount(len(products_list))
        for i, product in enumerate(products_list):
            id_producto, nombre_producto, precio, nombre_categoria = product
            self.productsTable.setItem(i, 0, QTableWidgetItem(str(id_producto)))
            self.productsTable.setItem(i, 1, QTableWidgetItem(str(nombre_producto)))
            self.productsTable.setItem(i, 2, QTableWidgetItem(str(precio)))
            self.productsTable.setItem(i, 3, QTableWidgetItem(str(nombre_categoria)))
    
    # Función para obtener el ID de la categoría seleccionada
    def get_selected_category_id(self):
        selected_category_text = self.comboBoxCategory.currentText()
        categories_list = self._product_model.get_categories()  # Cambio aquí
        for category in categories_list:
            if category[1] == selected_category_text:
                return category[0]
        return None

    # Función para leer la información de las categorías y cargar en el formulario
    def load_product_data(self, id_producto):
        self._product_id = id_producto
        product_data = self._product_model.get_product_by_id(id_producto)
        self.nameProductTextField.setText(product_data[1])
        self.priceProductTextField.setText(str(product_data[2]))
        categories_list = self._product_model.get_categories()
        self.comboBoxCategory.clear()  # Limpiar el combo box antes de agregar nuevas categorías
        self.comboBoxCategory.addItems([category[1] for category in categories_list])
        current_category_index = self.comboBoxCategory.findText(product_data[3])
        if current_category_index != -1:
            self.comboBoxCategory.setCurrentIndex(current_category_index)
        else:
            print(f"No se encontró la categoría: {product_data[3]}")

    # Función para limpiar el formulario
    def reset_form(self):
        self.nameProductTextField.clear()
        self.priceProductTextField.clear()
        self.comboBoxCategory.setCurrentIndex(-1) 
        self._product_id = None