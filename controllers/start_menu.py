from PyQt5.QtWidgets import QMainWindow
from PyQt5 import uic
from controllers.products import Products
from controllers.categories import Categories
from controllers.purchase_entry import purchaseEntry
from controllers.customers import Customers
import pathlib

class StartMenu(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        root_path = pathlib.Path(__file__).parent.parent
        uic.loadUi(root_path/ "views/start_menu.ui", self)
        self._products = Products()
        self._categories = Categories()
        self._purchase_entry = purchaseEntry()
        self._customers = Customers()

        self.productsButton.clicked.connect(lambda: self.get_products())
        self.categoriesButton.clicked.connect(lambda: self.get_categories())
        self.shoppingButton.clicked.connect(lambda: self.get_purchase_entry())
        self.customersButton.clicked.connect(lambda: self.get_customers())

    def get_products(self):
        self._products.show()

    def get_categories(self):
        self._categories.show()

    def get_purchase_entry(self):
        self._purchase_entry.show()

    def get_customers(self):
        self._customers.show()
    
