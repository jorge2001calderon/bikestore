import pathlib
from PyQt5.QtWidgets import QMainWindow, QWidget
from PyQt5.QtCore import Qt
from PyQt5 import uic

class purchaseEntry(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        route = pathlib.Path(__file__).parent.parent
        uic.loadUi(route/"views/purchase_entry.ui", self)