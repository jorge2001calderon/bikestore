from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QPushButton, QMessageBox
from PyQt5 import uic, QtCore
from models.categories_model import CategoriesModel
from controllers.categories_form import CategoriesForm
import pathlib

class Categories(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        route = pathlib.Path(__file__).parent.parent
        uic.loadUi(route/"views/categories.ui", self)
        self.__category_model = CategoriesModel()
        self._categoriesForm = CategoriesForm()
        self.load_categories()
        self.newCategoryAction.triggered.connect(lambda: self.create_category())
        self._categoriesForm.category_saved.connect(self.load_categories)

    #Función para cargar los datos en la tabla de categorías
    def load_categories(self):
        self.categoriesTable.clearContents()
        categories_list = self.__category_model.get_categories()
        self.categoriesTable.setRowCount(len(categories_list))
        for i, categoria in enumerate(categories_list):
            id_categoria, nombre_categoria = categoria
            self.categoriesTable.setItem(i, 0, QTableWidgetItem(str(id_categoria)))
            self.categoriesTable.setItem(i, 1, QTableWidgetItem(str(nombre_categoria)))
            self.categoriesTable.item(i, 0).setFlags(QtCore.Qt.ItemIsEnabled |QtCore.Qt.ItemIsSelectable)

            #Boton de Edición
            edit_button = QPushButton("Editar")
            edit_button.clicked.connect(self.edit_category)
            edit_button.setProperty("row", i)
            self.categoriesTable.setCellWidget(i, 2, edit_button)

            # Boton de Eliminar
            delete_button = QPushButton("Eliminar")
            delete_button.clicked.connect(self.delete_category_data)
            delete_button.setProperty("row", i)
            self.categoriesTable.setCellWidget(i, 3, delete_button)

    #Función de edición de categorías
    def edit_category(self):
        sender = self.sender()
        row = sender.property("row")
        id_category = self.categoriesTable.item(row, 0).text()
        self._categoriesForm.load_category_data(id_category)
        self._categoriesForm.show()

    # Función que prepara y muestra el formulario de creación de categorías
    def create_category(self):
        self._categoriesForm.reset_form()
        self._categoriesForm.show()

    # Función para eliminar una categoría y su respectivo mensaje de confirmación
    def delete_category_data(self):
        button = self.sender()
        row = self.categoriesTable.indexAt(button.pos()).row()
        id_category = self.categoriesTable.item(row, 0).text()
        msg = QMessageBox.question(
            self, 'Confirmar Eliminación', '¿Estás seguro que quieres eliminar esta categoría?',
            QMessageBox.Yes | QMessageBox.No
        )
        if msg == QMessageBox.Yes:
            try:
                if self.__category_model.delete_category(id_category):
                    self.load_categories()
                else:
                    QMessageBox.warning(self, 'Error', 'Hubo un problema al intentar eliminar la categoría.')
            except Exception as e:
                QMessageBox.critical(self, 'Error', f'Error al eliminar la categoría: {str(e)}')
    
    #Función para cerrar la conexión
    def closeEvent(self, ev) -> None:
        self.__category_model.close()
        return super().closeEvent(ev)