from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import pyqtSignal
from PyQt5 import uic
import pathlib
from models.categories_model import CategoriesModel

class CategoriesForm(QMainWindow):
    category_saved = pyqtSignal()

    def __init__(self) -> None:
        super().__init__()
        self._category_model = CategoriesModel() 
        self.category_id = None
        route = pathlib.Path(__file__).parent.parent
        uic.loadUi(route / "views/categories_form.ui", self)
        self.saveButton.clicked.connect(self.save_category)
        self.cancelButton.clicked.connect(self.close)

    #Función para guardar las categorías
    def save_category(self):
        try:
            # Verifica si es una actualización o una creación
            if self.category_id:
                self._category_model.update_category(
                    self.category_id,
                    self.nameCategoryTextField.text()
                )
            else:
                self._category_model.create_category(
                    self.nameCategoryTextField.text()
                )
            self.category_saved.emit()
            self.close()
        except Exception as e:
            print(f"Error al guardar la categoría: {str(e)}")

    # Función para leer la información de las categorías y cargar en el formulario
    def load_category_data(self, id_categoria):
        self.category_id = id_categoria
        category_data = self._category_model.get_category_by_id(id_categoria)
        self.nameCategoryTextField.setText(category_data[1])
        
    # Función para limpiar el formulario
    def reset_form(self):
        self.nameCategoryTextField.setText("")
        self.category_id = None
