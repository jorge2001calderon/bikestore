from models.store_model import StoreModel

class ProductsModel:
    def __init__(self):
        # Inicialización de la conexión y el cursor al crear una instancia de la clase
        sm = StoreModel()
        self._conn = sm.connection
        self._cur = sm.cursor

    # Función para obtener todos los productos
    def get_products(self):
        query = """
            SELECT id_producto, nombre_producto, precio, nombre_categoria
            FROM producto
            INNER JOIN categoria ON producto.id_categoria1 = categoria.id_categoria
            ORDER BY nombre_producto ASC
        """
        self._cur.execute(query)
        return self._cur.fetchall()
    
    # Función para crear un nuevo producto
    def create_product(self, nombre_producto, precio, id_categoria):
        query = """
            INSERT INTO producto(nombre_producto, precio, id_categoria1)
            VALUES (%s, %s, %s)
        """
        self._cur.execute(query, (nombre_producto, precio, id_categoria))
        self._conn.commit()

    # Función para obtener todas las categorías
    def get_categories(self):
        query = "SELECT * FROM categoria ORDER BY nombre_categoria ASC"
        self._cur.execute(query)
        return self._cur.fetchall()
    
    # Función para actualizar un producto existente
    def update_product(self, id_producto, nombre_producto, precio, id_categoria):
        query = """
            UPDATE producto
            SET nombre_producto = %s, precio = %s, id_categoria1 = %s
            WHERE id_producto = %s
        """
        self._cur.execute(query, (nombre_producto, precio, id_categoria, id_producto))
        self._conn.commit()

    #Función para obtener la lista de productos con información de categoría
    def get_products_with_category(self):
        query = """
            SELECT producto.id_producto, producto.nombre_producto, producto.precio, categoria.nombre_categoria
            FROM producto
            JOIN categoria ON producto.id_categoria1 = categoria.id_categoria
            ORDER BY producto.nombre_producto ASC
        """
        self._cur.execute(query)
        return self._cur.fetchall()

    #Función para obtener información de un producto específico con su categoría
    def get_product_by_id(self, product_id):
        query = """
            SELECT producto.id_producto, producto.nombre_producto, producto.precio, categoria.nombre_categoria
            FROM producto
            JOIN categoria ON producto.id_categoria1 = categoria.id_categoria
            WHERE producto.id_producto = %s
        """
        self._cur.execute(query, (product_id,))
        return self._cur.fetchone()
    
    # Función para eliminar un producto por su categoría
    def delete_product(self, id_producto):
        query = "DELETE FROM producto WHERE id_producto = %s"
        self._cur.execute(query, (id_producto,))
        self._conn.commit()
        return True
    
    # Función para cerrar la conexión   
    def close(self):
        self._cur.close()
        self._conn.close()

        