import psycopg2

class StoreModel:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(StoreModel, cls).__new__(cls)
            try:
                cls._instance._conn = psycopg2.connect("dbname=trabajo_final user=jurek password=jurek7 host=localhost")
                cls._instance._cur = cls._instance._conn.cursor()
            except Exception as e:
                print(f"Hubo un error al conectarse con la base de datos: {e}")
                raise e
        return cls._instance

    @property
    def cursor(self):
        return self._instance._cur
    
    @property
    def connection(self):
        return self._instance._conn

    def close_connection(self):
        self._instance._cur.close()
        self._instance._conn.close()

        