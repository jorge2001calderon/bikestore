from models.store_model import StoreModel

class CategoriesModel:
    def __init__(self):
        # Inicialización de la conexión y el cursor al crear una instancia de la clase
        sm = StoreModel()
        self._conn = sm.connection
        self._cur = sm.cursor

    # Función para obtener todas las categorías y ordenadas por su nombre
    def get_categories(self):
        query = "SELECT * FROM categoria ORDER BY nombre_categoria"
        self._cur.execute(query)
        return self._cur.fetchall()

    # Función para crear una nueva categoría
    def create_category(self, nombre_categoria):
        query = "INSERT INTO categoria(nombre_categoria) VALUES (%s)"
        self._cur.execute(query, (nombre_categoria,))
        self._conn.commit()

    # Función para actualizar el nombre de una categoría dado su ID
    def update_category(self, id_categoria, nombre_categoria):
        query = "UPDATE categoria SET nombre_categoria = %s WHERE id_categoria = %s"
        self._cur.execute(query, (nombre_categoria, id_categoria))
        self._conn.commit()

    # Función para obtener información de una categoría por su ID
    def get_category_by_id(self, category_id):
        query = "SELECT * FROM categoria WHERE id_categoria = %s"
        self._cur.execute(query, (category_id,))
        return self._cur.fetchone()
    
    # Función para eliminar una categoría por su ID
    def delete_category(self, id_categoria):
        query = "DELETE FROM categoria WHERE id_categoria = %s"
        self._cur.execute(query, (id_categoria,))
        self._conn.commit()
        return True
    
    # Función para cerrar la conexión   
    def close(self):
        self._cur.close()
        self._conn.close()


