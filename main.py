import sys
from PyQt5.QtWidgets import QApplication
from controllers.start_menu import startMenu

app = QApplication(sys.argv)
psycopg_example = startMenu()
psycopg_example.show()
sys.exit(app.exec())
